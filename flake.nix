{
  description = "PyData Talk Demo";

  inputs = {
    nixpkgs = { url = "github:nixos/nixpkgs/nixpkgs-unstable"; };
    flake-utils = { url = "github:numtide/flake-utils"; };
  };

  outputs = { self, nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        appPkgs = with pkgs; [
          nixpkgs-fmt
          vhs
          btop
          tldr
          nb
          tig
          kubectl
          python3
          hyfetch
          neovim
          vim
          fish
          nushell
          ranger
        ];
        
        customScripts = [
          (pkgs.writeShellApplication {
            name = "rebuild-gif";
            runtimeInputs = [ ];
            text = ''
              #!/usr/bin/env bash
              set -e
              for file in vhs/*.tape; do vhs "$file"; done
            '';
          })
        ];
      in
      rec {

        devShells = {
          default = pkgs.mkShell {
            inputsFrom = [  ];
            nativeBuildInputs = appPkgs ++ customScripts ;
          };
        };

        devShell = devShells.default;

      });
}




