# Exploring the Magic of Command Line Tools and TUI's for Python Developers, Data Science Pros, and DevOps professionals"

This talk is for Python Developers, Data Science Pros, and DevOps folks. We'll dive into the magic of the command line and TUI's, showing how awesome text files are and how AI can spice up editors. We'll chat about why advanced tools rock more than basic ones, the coolness of open source software, and why testing tools matter. If we've got time, we'll explore quirky stuff like color themes, custom linux interfaces, and funky plugins. We'll wrap up with tips on using these tools on your own laptop and at work, introducing Nix ecosystem as a sweet solution for using these tools.

## Slides
https://docs.google.com/presentation/d/11-ljj0ZiRKo2mARQGIWvWiPOJnOXo_AYufde7QLJIWY/edit?usp=sharing

## Requirements

### Nix Package Manager

To install run the following command on any generic linux system:
```
sh <(curl -L https://nixos.org/nix/install) --daemon
```

For more info on nix: 
https://github.com/mikeroyal/NixOS-Guide

## Setup

### Enter the environment

```
nix develop
```

### Rebuild the gifs
Recreate the tooling gifs
```
vhs vhs/<gifname>.tape
```

### Structure
- All the gifs use `style.tape`. 
- nix shell is run before the recording starts
- NOTE: this assumes there is a `~/justin-nix` checkout 

## Generic Shell Tools
These ones don't care if you a specific shell or editor.

### btop
Improved top command that shows lots of system information.
![btop](vhs/btop.gif)

### tldr
A collection of simplified and community-driven man pages.

![tldr](vhs/tldr.gif)

### neofetch / hyfetch 
hyfetch - neofetch with flags <3

i.e. show various system info

![hyfetch](vhs/hyfetch.gif)

![hyfetch](vhs/hyfetch-flag.gif)


### Zoxide 
A smarter cd command

Remembers your visited directories and goes to the match

![Zoxide](vhs/zoxide.gif)

### Tig
text-mode interface for Git

![Tig](vhs/tig.gif)

### Oh My Git!
[Website](https://ohmygit.org/)

[Game Download](https://blinry.itch.io/oh-my-git)

### nb

![nb](vhs/nb.gif)

### ripgrep
- [github](https://github.com/BurntSushi/ripgrep)

### fd
- [github](https://github.com/sharkdp/fd)

### Fzf
Terminal fuzzy finder
- [github](https://github.com/junegunn/fzf)

### Fish
![fish-suggestions](vhs/fish-suggestions.gif)

NOTE: this is with fzf installed  
![fish-history](vhs/fish-history.gif)

### Zsh
- If you have any issues with fish shell, this is a good backup
- Well supported
- Oh my zsh is a must
- https://ohmyz.sh/

### NuShell
Everything is data

[website](https://www.nushell.sh/)

![ls-example](img/ls-example.png)


### Vim / NeoVim

![keybindings](img/vim.en.png)

#### Telescope
![telescope](vhs/telescope.gif)

![telescope-git](vhs/telescope-git.gif)

#### ChatGpt
![chatgpt](vhs/chatgpt.gif)

![chatgpt_explain](vhs/chatgpt_explain.gif)

#### Cellular Automata

![cell](vhs/cell.gif)

[![Good example](http://img.youtube.com/vi/W8NfG6osJdI/0.jpg)](https://youtu.be/W8NfG6osJdI)

![github](https://github.com/Eandrju/cellular-automaton.nvim)

#### Misc Vim

- [LazyNvim](https://github.com/folke/lazy.nvim)

- [Vim Adventures](https://vim-adventures.com/)
- [Vim Vscode Plugin](https://github.com/VSCodeVim/Vim)

Both of these tools let you use vim keybindings on the web
- [Vimium C](https://github.com/gdh1995/vimium-c)
- [qutebrowser](https://qutebrowser.org/)
- [Awesome Vim bindings](https://github.com/erikw/vim-keybindings-everywhere-the-ultimate-list)



#### Ranger
File navigation with Vim keybindings

![ranger](vhs/ranger.gif)

### Splits and Terminal Emulators
#### Tmux (terminal multiplexer)
- Splits are done in the shell
- More portable but with more problems
#### Kitty
- Terminal emulator for linux both on wayland and x11
- Also on macos 
- Allows for similar style tmux splits 
#### iTerm2
Another good terminal emulator for macOS only



### Ollama
- Gen.nvim (similar to other plugin but with ollama backend)
- https://github.com/David-Kunz/gen.nvim
- Local models that are easy to manage.
- Much more private
- If you have not started coding with AI and privacy is the main reason not too, look into this.
- Pull hugging face models
- Manages large artifacts like docker, ex docker devs created it
- I’m quite a fan of ollama. I think not paying for api calls can allow people to do a lot with their hardware
- Some sort of editor integration
- Both completion and bespoke prompting is really useful!

### Testing

#### Pytest Plugins on PyPi
- **Pytest-sugar** - This plugin is what gives the nice colors are icons
- **pytest-watch** - Gives you the ptw -c command
    This will rerun your tests when there is a file test
    Adding stuff like this to your workflow can speed things up!
- **Pytest-testmon** - pytest --testmon. This command will magically only rerun the tests that actually had code changed 


### nix-shell
![nix-shell](vhs/nix-shell.gif)

### What if I want this in my environment all the time?
#### Add the nix-shell command you want to your .bashrc or .fishrc file
Simple and easy!
#### Use home manager to manage user packages and dotfiles 
https://github.com/nix-community/home-manager
A repo with a home manager setup is the best dotfile repo imo for 2024
#### Use nixos and get a persistent config!
https://gitlab.com/slenderq/justin-nix
All my configs are public if you would like inspiration!

